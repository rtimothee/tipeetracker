//
//  UserDataClass.swift
//  TipeeTracker
//
//  Created by Timothee Roldao on 16/09/2023.
//
import SwiftUI

class UserData: ObservableObject {
    @Published var isLogged: Bool = false
    @Published var showLogin: Bool = false
    
    @Published var login: String = ""
    @Published var password: String = ""
    @Published var url: String = ""
    
    public func initLogin() {
        // get Login Datas
        let keychain = KeychainService.shared
        if let login = keychain.retrieve(for: keychain.accountLogin),
           let password = keychain.retrieve(for: keychain.accountPassword),
           let url = keychain.retrieve(for: keychain.accountURL) {
            
            print(login);
            print(password);
            print(url);
            self.login = login
            self.password = password
            self.url = url
            self.isLogged = true
        }
    }
    
}
