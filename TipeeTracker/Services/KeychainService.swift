//
//  KeychainService.swift
//  TipeeTracker
//
//  Created by Timothee Roldao on 17/09/2023.
//

import Security
import SwiftUI

class KeychainService {
    static let shared = KeychainService()
    
    private let appName: String = "TipeeTracker"
    private let service = "com.TimotheeRoldao.TipeeTracker"
    public let accountLogin = "login"
    public let accountPassword = "password"
    public let accountURL = "url"
    
    func store(_ value: String, for account: String) -> Bool {
        if let data = value.data(using: .utf8) {
            let query: [String: Any] = [
                kSecClass as String: kSecClassGenericPassword,
                kSecAttrService as String: service,
                kSecAttrAccount as String: account,
                kSecValueData as String: data
            ]
            
            SecItemDelete(query as CFDictionary) // Supprimer l'élément existant s'il existe
            let status = SecItemAdd(query as CFDictionary, nil)
            if status != errSecSuccess {
                return false
            }
            
            return true
        }
        return false
    }
    
    func retrieve(for account: String) -> String? {
        let query: [String: Any] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrService as String: service,
            kSecAttrAccount as String: account,
            kSecReturnData as String: kCFBooleanTrue!,
            kSecMatchLimit as String: kSecMatchLimitOne
        ]
        
        var dataTypeRef: AnyObject?
        let status = SecItemCopyMatching(query as CFDictionary, &dataTypeRef)
        
        if status == errSecSuccess {
            if let retrievedData = dataTypeRef as? Data,
               let value = String(data: retrievedData, encoding: .utf8) {
                return value
            }
        }
        return nil
    }
    
    func delete(for account: String) {
        let query: [String: Any] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrService as String: service,
            kSecAttrAccount as String: account
        ]
        
        SecItemDelete(query as CFDictionary)
    }
}
