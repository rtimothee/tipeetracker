//
//  SwiftUIView.swift
//  TipeeTracker
//
//  Created by Timothee Roldao on 16/09/2023.
//

import SwiftUI

struct LoginView: View {
  @State private var login = ""
  @State private var password = ""

  var body: some View {
    Form {
      TextField("Login", text: $login)
      TextField("Password", text: $password)

      Button("Login") {
        if authenticate(login: login, password: password) {
          // Connexion réussie
        } else {
          // Connexion échouée
        }
      }
    }
  }

  private func authenticate(login: String, password: String) -> Bool {
    let defaults = UserDefaults.standard
    return defaults.string(forKey: "login") == login && defaults.string(forKey: "password") == password
  }
}
struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
