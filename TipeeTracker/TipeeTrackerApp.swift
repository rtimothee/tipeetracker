//
//  TipeeTrackerApp.swift
//  TipeeTracker
//
//  Created by Timothee Roldao on 16/09/2023.
//

import SwiftUI
import Security

@main
struct TipeeTrackerApp: App {
    
    @StateObject private var userData = UserData()
    @State private var isLoaded: Bool = Bool()

    init() {
        self.userData.initLogin()
    }
    
    var body: some Scene {
            MenuBarExtra("UtilityApp",
                         systemImage: self.userData.isLogged ? "eye" : "eye.trianglebadge.exclamationmark" // TODO
            ) {
                    AppMenu().environmentObject(UserData())
            }
        
        Window("Login", id: "LoginWindow") {
            LoginView().frame(width: 250.0, height: 250.0)
        }
    }
    
}
