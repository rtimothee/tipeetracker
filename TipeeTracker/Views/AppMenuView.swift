//
//  AppMenu.swift
//  TipeeTracker
//
//  Created by Timothee Roldao on 16/09/2023.
//

import SwiftUI

struct AppMenu: View {
    @EnvironmentObject var userData: UserData
    @Environment(\.openWindow) var openWindow
    
    var body: some View {
        if self.userData.isLogged{
            Button(action: action1, label: { Text("Action 1") })
            Button(action: action1, label: { Text("Action 2") })
            Divider()
            Button(action: action1, label: { Text("Action 3") })
        } else {
            Button(action: openLoginView, label: { Text("Connexion") })
        }
        Divider()
        Button("Quitter l'application") {
            NSApplication.shared.terminate(self)
        }
        
    }
    
    func action1() {
        print("tamere")
        self.userData.isLogged = !self.userData.isLogged
    }
    
    func openLoginView() {
        openWindow(id: "LoginWindow")
        self.userData.showLogin.toggle()
            
    }
}

struct AppMenu_Previews: PreviewProvider {
    static var previews: some View {
        AppMenu()
    }
}
