//
//  LoginView.swift
//  TipeeTracker
//
//  Created by Timothee Roldao on 16/09/2023.
//

import SwiftUI

struct LoginView: View {
    @State private var url = ""
    @State private var login = ""
    @State private var password = ""
    @Environment(\.dismiss) private var dismiss


  var body: some View {
      Form {
        TextField("URL", text: $url)
        TextField("Login", text: $login)
        TextField("Password", text: $password)

          Button("Cancel") {
           dismiss()
          }
        Button("Connect") {
            if authenticate(login: login, password: password, url: url) {
            print("OK")
          } else {
            print("pas OK")
          }
        }
      }
      .padding(20.0)
      .frame(width: 250.0, height: 150.0)
  }

    private func authenticate(login: String, password: String, url: String) -> Bool {
      let keychain = KeychainService.shared
      
      if keychain.store(login, for: keychain.accountLogin),
      keychain.store(password, for: keychain.accountPassword),
         keychain.store(url, for: keychain.accountURL) {
          return true
      }
          
     return false
  }
}
struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
